# 1. Create a Class called Camper and give it the attributes name, batch, course_type

# 2. Create a method called career_track which will print out the string Currently enrolled in the <value of course_type> program.

# 3. Create a method called info which will print out the string My name is <value of name> of batch <value of batch>.

# 4. Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type.

# 5. Print the value of the object's name, batch, course type.

# 6. Execute the info method and career_track of the object.

class Camper():
    # Item No. 1
    def __init__(self, name, batch, course_type):
        self.name = name
        self.batch = batch
        self.course_type = course_type
    # Item No. 2

    def career_track(self):
        print(f"Currently enrolled in {self.course_type}")
    # Item No. 3

    def info(self):
        print(f"My name is {self.name} of batch {self.batch}")


# Item No. 4
zuitt_camper = Camper("Edward", "359", "Python Short Course")

# Item No. 5
# print(vars(zuitt_camper))
print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Name: {zuitt_camper.batch}")
print(f"Camper Name: {zuitt_camper.course_type}")

# Item No. 6
zuitt_camper.info()
zuitt_camper.career_track()
